package com.piatras.datalogger;

import java.util.Date;
import java.text.SimpleDateFormat;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;

public class TemperatureRecord {
    private final ObjectProperty<Date> date;
    private final StringProperty dateString;
    private final FloatProperty time;
    private final FloatProperty internalTemperature;
    private final FloatProperty thermocoupleTemperature;

    public static final SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public TemperatureRecord(Date _date, float _thermocoupleTemperature, float _internalTemperature) {    
        this.date = new SimpleObjectProperty<Date>(_date);
        this.dateString = new SimpleStringProperty(_date.toString());
        this.time = new SimpleFloatProperty(0.0f);
        this.thermocoupleTemperature = new SimpleFloatProperty( _thermocoupleTemperature );
        this.internalTemperature = new SimpleFloatProperty( _internalTemperature );
    }

    public Date getDate(){
        return date.get();
    }

    public void setDate(Date d){
        this.date.set(d);
    }

    public float getTime(){
        return time.get();
    }

    public void setTime(float t){
        this.time.set(t);
    }

    public float getThermocoupleTemperature(){
        return thermocoupleTemperature.get();
    }

    public void setThermocoupleTemperature(float temp){
        this.thermocoupleTemperature.set(temp);
    }

    public float getInternalTemperature(){
        return internalTemperature.get();
    }

    public void setInternalTemperature(float temp){
        this.internalTemperature.set(temp);
    }

    public ObjectProperty dateProperty(){
        return this.date;
    }

    public FloatProperty timeProperty(){
        return this.time;
    }
    
    public FloatProperty internalTemperatureProperty(){
        return this.internalTemperature;
    }

    public FloatProperty thermocoupleTemperatureProperty(){
        return this.thermocoupleTemperature;
    }

    public StringProperty dateStringProperty(){
        return this.dateString;
    }

    public String toString(){
        return date_format.format(date.get()) + " (" + time.get() + "s), " + thermocoupleTemperature.get() + ", " + internalTemperature.get();
    }
}