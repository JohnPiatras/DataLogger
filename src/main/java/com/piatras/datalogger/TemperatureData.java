package com.piatras.datalogger;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.io.StringReader;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.util.Date;
import java.lang.Float;

import java.util.regex.Pattern;

import java.text.ParseException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.beans.value.ObservableValue;

public class TemperatureData implements Iterable<TemperatureRecord>{

    private ArrayList<TemperatureRecord> data = new ArrayList<TemperatureRecord>();
    private ObservableList<TemperatureRecord> dataObservableList = FXCollections.observableArrayList();

    private TemperatureData(){
    }

    private static TemperatureRecord parseRecord(String[] record_string) throws ParseException, NumberFormatException {
        Date d = TemperatureRecord.date_format.parse(record_string[0] + " " + record_string[1]);
        float thermocoupleTemperature = Float.parseFloat(record_string[2]);
        float internalTemperature = Float.parseFloat(record_string[3]);
        TemperatureRecord t = new TemperatureRecord(d, thermocoupleTemperature, internalTemperature);
        return t;
    }

    public void SaveToCSV(String path){
        try(
            Writer writer = Files.newBufferedWriter(Paths.get(path));
            CSVWriter csvWriter = new CSVWriter(writer,
                CSVWriter.DEFAULT_SEPARATOR,
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);
        ){
            String[] headerRecord = {"Date", "Time", "Thermocouple", "Internal Temp"};
            csvWriter.writeNext(headerRecord);
            for(TemperatureRecord t : data){  
                String date = TemperatureRecord.date_format.format(t.getDate());
                String time = date.substring(11);
                date = date.substring(0, 10);
                float thermocouple_temp = t.getThermocoupleTemperature();
                float internal_temp = t.getInternalTemperature();
                

                csvWriter.writeNext(new String[] {date, time, Float.toString(thermocouple_temp), Float.toString(internal_temp)});
            }
        }catch(Exception e){
            System.out.println("Error writing out to file " + path);
            System.out.println(e.getMessage());
        }  

    }

    public static TemperatureData LoadFromCSV(String path) throws IOException {
        TemperatureData temperatureData = new TemperatureData();
        try (
            Reader reader = Files.newBufferedReader(Paths.get(path));
            CSVReader csvReader = new CSVReader(reader);
        ) {
            // Reading Records One by One in a String array
            String[] record;
            int line_no = 1;
            Date basetime = null; //time of first temperature measurement
            while ((record = csvReader.readNext()) != null) {
                try{
                    TemperatureRecord t = parseRecord(record);
                    if(basetime == null)basetime = t.getDate();

                    t.setTime( (float)( t.getDate().getTime() - basetime.getTime()) / 1000.0f );

                    temperatureData.data.add(t);
                    temperatureData.dataObservableList.add(t);
                }catch(Exception e){
                    System.out.println("Error parsing record at line " + line_no + ": ");
                    for(int i = 0; i < record.length; i++){
                        System.out.println("  Field " + i + ": " + record[i]);
                    }
                    System.out.println("Error: " + e + "\n");
                }
                line_no++;
            }
            return temperatureData;
        }
    }

    public static TemperatureData LoadFromCSVString(String csvstring) throws IOException {
        TemperatureData temperatureData = new TemperatureData();
        try (
            Reader reader = new StringReader(csvstring);//Files.newBufferedReader(Paths.get(path));
            CSVReader csvReader = new CSVReader(reader);
        ) {
            // Reading Records One by One in a String array
            String[] record;
            int line_no = 1;
            Date basetime = null; //time of first temperature measurement
            while ((record = csvReader.readNext()) != null) {
                try{
                    TemperatureRecord t = parseRecord(record);
                    if(basetime == null)basetime = t.getDate();

                    t.setTime( (float)( t.getDate().getTime() - basetime.getTime()) / 1000.0f );

                    temperatureData.data.add(t);
                    temperatureData.dataObservableList.add(t);
                }catch(Exception e){
                    System.out.println("Error parsing record at line " + line_no + ": ");
                    for(int i = 0; i < record.length; i++){
                        System.out.println("  Field " + i + ": " + record[i]);
                    }
                    System.out.println("Error: " + e + "\n");
                }
                line_no++;
            }
            return temperatureData;
        }
    }

    @Override
    public Iterator<TemperatureRecord> iterator(){
        return data.iterator();
    }
    
    public ObservableList<TemperatureRecord> getObservableList(){
        return dataObservableList;
    }


}