package com.piatras.datalogger;

import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class DownloadTask extends Task<String[]>{
    
    private final String commPort;

    public DownloadTask(String _commPort){
        this.commPort = _commPort;
    }

    @Override
    public String[] call() {
        updateMessage("Connecting...");
        InstrumentComms instrument = InstrumentComms.connect(this.commPort);
        if(instrument != null){
            updateMessage("Connected to instrument on " + instrument.getPortName());
            instrument.startDownload();
            
            int recvBytes = 0;
            int totalBytes = 0;
            do{
                recvBytes = instrument.receiveData();
                totalBytes += recvBytes;
                updateMessage("Downloaded " + totalBytes + " bytes.");
            }while(!isCancelled() &&  recvBytes > 0);
            
            
            instrument.close();
            return new String[] {instrument.getPortName(), instrument.getData()};
        }
        updateMessage("Error: Unable to connect to instrument.");

        return null;
    }
}
