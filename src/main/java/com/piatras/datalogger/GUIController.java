package com.piatras.datalogger;

import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.application.Platform;

import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.layout.VBox;
import javafx.scene.Node;

import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;

import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.stage.Stage;

import javafx.event.ActionEvent;

import java.io.IOException;
import java.io.File;

import java.util.Date;

import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.concurrent.WorkerStateEvent;

import javafx.scene.control.Dialog;
import javafx.util.Pair;
import java.util.Optional;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ButtonBar.ButtonData;

public class GUIController{
    private Stage stage;
    private boolean exit = false;
    private Dialog<ButtonType> downloadDialog;
    private TemperatureData temperature_data = null;
    private String instrumentCommPort = null;

    @FXML 
    private LineChart<Number, Number> linechart;

    @FXML
    private NumberAxis xAxis;

    @FXML
    private NumberAxis yAxis;

    @FXML
    private TableView<TemperatureRecord> temperatureTable;

    @FXML
    private TableColumn<TemperatureRecord, String> dateColumn;

    @FXML 
    private TableColumn<TemperatureRecord, Number> timeColumn;

    @FXML 
    private TableColumn<TemperatureRecord, Number> thermocoupleTemperatureColumn;

    @FXML 
    private TableColumn<TemperatureRecord, Number> internalTemperatureColumn;

    @FXML
    private void initialize() throws IOException{
        dateColumn.setCellValueFactory(cellData -> cellData.getValue().dateStringProperty());
        timeColumn.setCellValueFactory(cellData -> cellData.getValue().timeProperty());
        thermocoupleTemperatureColumn.setCellValueFactory(cellData -> cellData.getValue().thermocoupleTemperatureProperty());
        internalTemperatureColumn.setCellValueFactory(cellData -> cellData.getValue().internalTemperatureProperty());
        //doDataDownload();
        initDownloadDialog();
    }
     
    private void initDownloadDialog(){
        downloadDialog = new Dialog<>();
        downloadDialog.setTitle("Downloading Data");
        downloadDialog.setContentText("Connecting...");
        //downloadDialog.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL);

        ButtonType cancelButtonType = new ButtonType("Cancel", ButtonData.OK_DONE);
        downloadDialog.getDialogPane().getButtonTypes().addAll(cancelButtonType);
    }


    private void doDataDownload() {
        
        Task<String[]> downloadTask = new DownloadTask(instrumentCommPort);

        downloadTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                try{
                    String[] result = downloadTask.getValue();
                    instrumentCommPort = result[0];
                    temperature_data = TemperatureData.LoadFromCSVString(result[1]);
                    downloadDialog.close();
                    displayData(temperature_data);
                }catch(IOException e){
                    System.out.println("Error downloading data!");
                    e.printStackTrace();
                }catch(Exception e){
                    System.out.println(e);
                }
            }
        });

        downloadTask.messageProperty().addListener((obs, oldMsg, newMsg) -> {
            downloadDialog.setContentText(newMsg);

        });

        Thread downloadThread = new Thread(downloadTask);
        downloadThread.start();

        Optional<ButtonType> result = downloadDialog.showAndWait();
        if (result.isPresent() && result.get().getButtonData() == ButtonData.OK_DONE){
            downloadTask.cancel();
        }

    }

    private void displayData(TemperatureData temp_data){
        Date start_time = null;
        XYChart.Series<Number, Number> dataSeries1 = new XYChart.Series<Number, Number>();
        for(TemperatureRecord t : temp_data){
            if(start_time == null)start_time = t.getDate();
            float time = t.getTime();
            float temp = t.getThermocoupleTemperature();
            dataSeries1.getData().add(new XYChart.Data<Number, Number>( time, temp));
        }
        linechart.setTitle("Skin temperature (" + TemperatureRecord.date_format.format(start_time) + ")");
        linechart.getData().clear();
        linechart.getData().add(dataSeries1);
        
        temperatureTable.setItems(temp_data.getObservableList());
        //linechart.setData(dataSeries1);
    }

    @FXML
    private void onFileOpen(ActionEvent ae){
        MenuItem fileOpen = (MenuItem) ae.getSource();
        Window stage = fileOpen.getParentPopup().getScene().getWindow();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open CSV file");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(stage);
        if(file != null){
            try{
                temperature_data = TemperatureData.LoadFromCSV(file.getAbsolutePath());
                displayData(temperature_data);
            }catch(Exception e){
                System.out.println("Error loading data from file.");
                System.out.println(e);
            }
        }
    }

    @FXML
    private void onFileSaveAs(ActionEvent ae){
        if(temperature_data == null)return;

        MenuItem fileSave = (MenuItem) ae.getSource();
        Window stage = fileSave.getParentPopup().getScene().getWindow();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        

        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setTitle("Save data as CSV file");
        File file = fileChooser.showSaveDialog(stage);
        if(file != null){
            try{
                System.out.println("Saving to " + file.getAbsolutePath());
                temperature_data.SaveToCSV(file.getAbsolutePath());
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
        }
    }

    @FXML
    private void onExit(){
        //outputLabel.setText("Press exit again to exit...");
        //if(exit)
        //if(instrument != null)instrument.close();
        Platform.exit();
        //exit = true;
    }

    @FXML
    private void onMenuDownload(){
        doDataDownload();
    }

    public void setStage(Stage s){
        stage = s;
    }


}