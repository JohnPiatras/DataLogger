package com.piatras.datalogger;

import com.opencsv.CSVReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.*;

//.Files;
//import java.nio.file.Paths;

/*public class DataLogger {
    public static void main(String[] args){
        System.out.println("Hello World!");
    }
}*/


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Parent;
import javafx.scene.control.Label;


import java.net.URL;

public class DataLogger extends Application {
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/MainGUI.fxml"));
        Parent root = (Parent)loader.load();
        Scene scene = new Scene(root);

        GUIController guiController = loader.getController();
        guiController.setStage(primaryStage);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    public static void main(String[] args) throws IOException {
        //loadCSV.load("../../../sample_data/SHS.csv");

        //TemperatureData temp_data = TemperatureData.LoadFromCSV("../../../sample_data/test0509.csv");
       

        Application.launch(DataLogger.class, args);
    }
    

}