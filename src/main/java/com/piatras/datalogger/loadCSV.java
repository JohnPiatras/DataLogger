package com.piatras.datalogger;

import com.opencsv.CSVReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.*;
import java.util.ArrayList;

public class loadCSV {

    private ArrayList<String[]> records = new ArrayList<String[]>();;



    public void load(String path) throws IOException {
        try (
            Reader reader = Files.newBufferedReader(Paths.get(path));
            CSVReader csvReader = new CSVReader(reader);
        ) {
            // Reading Records One by One in a String array
            String[] nextRecord;
            while ((nextRecord = csvReader.readNext()) != null) {
                for(int i = 0; i < nextRecord.length; i++){
                    //System.out.print(nextRecord[i] + ", ");
                    records.add(nextRecord);
                }
                //System.out.println();
            }
        }

    }

    public ArrayList<String[]> getRecords(){
        return records;
    }
}