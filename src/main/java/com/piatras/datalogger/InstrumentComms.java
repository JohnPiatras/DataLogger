package com.piatras.datalogger;

import com.fazecast.jSerialComm.*;
import java.lang.StringBuilder;


public class InstrumentComms {
    private static final int baud_rate = 74880;
    private static final int data_bits = 8;
    private static final int stop_bits = 1;
    private static final int parity = SerialPort.NO_PARITY;

    private static final byte[] CMD_READY = {'O', 'S', 'W', 'W', 'M', ':', '0'};
    private static final byte[] CMD_DOWNLOAD = {'O', 'S', 'W', 'W', 'M', ':', '1'};
    private static final String RESP_READY = "OSWWM:OK";

    //instance variables
    private final SerialPort port;
    private StringBuilder data = null;
    private boolean isDownloading = false;

    private InstrumentComms(SerialPort s){
        this.port = s;
    }

    public void startDownload(){
        if(port.isOpen()){
            System.out.println("**Starting download...");
            port.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 1000, 0);
            port.writeBytes(CMD_DOWNLOAD, 7);

            data = new StringBuilder();
        }else{
            System.out.println("Unable to download, port is closed");
        }
    }

    public int receiveData(){
        byte[] buffer = new byte[1024];
        int bytesRead = 0;
        if(port.isOpen()){
            bytesRead = port.readBytes(buffer, buffer.length);

            System.out.println("Received " + bytesRead + " bytes");
            data.append(new String(buffer, 0, bytesRead));
            }else{
            System.out.println("Unable to receive data, port is closed");
        }
        return bytesRead;       
    }

    public String getData(){
        if(data != null)
            return data.toString();
        else
            return null;
    }

    public String getPortName(){
        String portname = "";
        if(port != null)portname = port.getSystemPortName();
        return portname;
    }

    public void close(){
        port.closePort();
    }


    //--------------Static methods-----------------
    public static void flush(SerialPort s){
        byte[] buf = new byte[1024];
        int nbytes = 0;
        if(s.isOpen()){            
            do{
                nbytes = s.readBytes(buf, 1024);
                System.out.println("Flushed " + nbytes + " bytes");
            }while(nbytes > 0);
            System.out.println("Port flushed");
        }else{
            System.out.println("Unable to flush, port is closed");
        }
        
    }


    public static InstrumentComms connect(String commPortName){
        SerialPort[] port_list = SerialPort.getCommPorts();
        InstrumentComms instrument = null;

        System.out.println("Found " + port_list.length + " serial ports");
        if(commPortName != null)System.out.println("Expect instrument on " + commPortName);
        for(SerialPort s : port_list){
            if(commPortName != null && !s.getSystemPortName().equals(commPortName))continue;

            System.out.println("\nChecking port: " + s.getSystemPortName());
            s.setComPortParameters(baud_rate, data_bits, stop_bits, parity);
            s.openPort();
            s.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 3000, 0);

            //flush the serial port
            flush(s);
            
            s.writeBytes(CMD_READY, 7);
            byte[] readBuffer = new byte[8];
            int numRead = s.readBytes(readBuffer, readBuffer.length);
            String response = new String(readBuffer);
            //s.closePort();
            System.out.println("  Got response: [" + response + "]");
            if(response.equals(RESP_READY)){
                System.out.println("Instrument found on " + s.getDescriptivePortName());
                instrument = new InstrumentComms(s);
                break;
            }else{
                System.out.println("No instrument on " + s.getSystemPortName());
            }
            
        }


        return instrument;
        //return new InstrumentComms(instrument_port);
    }







}